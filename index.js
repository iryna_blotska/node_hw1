const express = require('express');
const path = require('path');
const app = express();
const fs = require('fs');

app.use(express.json());
app.use((req, res, next) => {
	console.log(req.method);
	console.log(req.url);
	next();
})
app.get('/api/files', (req, res) => {
	const getFiles = async () => {
		try {
			await fs.readdir('./api/files', (err, files) => {
				res.status(200).send({
					"message": "Success",
					"files": files
				})
			});
		} catch (err) {
			res.status(500).send({ "message": "Server error"});
		}
	};
	getFiles();
});

app.get('/api/files/:filename', (req, res) => {
	const filename = req.params.filename;
	const getFile = async () => {
		try {
			await fs.readFile('./api/files/' + filename, 'utf8', (err, data) => {
				if(err){
					return res.status(400).send({ 'message': 'No file with '+ filename +' found' });
				}
				const mtime = fs.statSync('./api/files/' + filename).mtime;
				res.status(200).send({
					"message": "Success",
					"filename": filename,
					"content": data,
					"extension": path.extname(filename).slice(1),
					"uploadedDate": mtime
				})
			});
		} catch (err) {
			res.status(500).send({ "message": "Server error"});
		}
	};
	getFile();
})

app.post('/api/files', (req, res) => {
	const createFile = async () => {
		try {
			if(req.body.filename === undefined){
				return res.status(400).json({ 'message': 'Please specify \'filename\' parameter' });
			}
			if(req.body.content === undefined || req.body.content.length <= 0){
				return res.status(400).json({ 'message': 'Please specify \'content\' parameter' });
			}
			if(!req.body.filename.match(/([a-zA-Z0-9\s_\\.\-\(\):])+(\.txt|\.js|\.json|\.log|\.yaml|\.xml)$/)){
				return res.status(400).json({ 'message': 'Please specify \'file\' extension' });
			}
			await fs.writeFile('./api/files/' + req.body.filename, req.body.content, 'utf8', () => {
				res.status(200).json({ 'message': 'File created successfully' });
			});
		} catch (err) {
			res.status(500).json({ 'message': 'Server error' });
		}

	};
	createFile();
});

app.listen(8080);